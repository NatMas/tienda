@extends('layouts.app')
@section('content')
<style>
    .container{
        padding-left: 50%;
    }

    .card-body{
        margin-top: 10px;
    }
</style>
<div class="card-header">


        <form method="get" action="/productsmin">
            <input name="price" placeholder="Buscar por minimo" type="text">
            <button name="Busca" type="submit">Coprovar minim</button>
        </form>

        <form method="get" action="/productsmax">
            <input name="price" placeholder="Buscar por maximo" type="text">
            <button name="Busca" type="submit">Comprovar maxim</button>
        </form>
    <form method="get" action="/productspreu">
        <input name="price" placeholder="Buscar por precio" type="text">
        <button name="Busca" type="submit">Buscar per preu</button>
    </form>
    <form method="get" action="/products">
        <button name="Busca" type="submit">Reset</button>
    </form>

    <br>
    </div>
    <div class="container">

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Products') }}</div>
                <!--{--{dd($products)}}-->
                    <div class="card-body">
                        @foreach ($products as $product)
                            <li>Name: {{$product->name}}</li>
                            <li>Price: {{$product->price}}</li>
                            <li>Stock: {{$product->stock}}</li>
                            <img style="height: 50px" src="data:image/jpeg;base64,{!! stream_get_contents($product->image) !!}"/>
                            <br>
                            <p style="background: #ffd47c; width: 50px" class="btn-holder"><a href="{{ url('add-to-cart/'.$product->id) }}" class="btn btn-warning btn-block text-center" role="button">añadir</a> </p>
                            <br>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
{{$products->links()}}
@endsection
