<?php
return[
    'Welcome' => 'Bienvenido',
    'Register' => 'Registro',
    'Name'=>'Nombre',
    'Surname'=>'Apellido',
    'Password'=>'Contraseña',
    'Confirm Password' => 'Confirmar Contraseña',
    'Email'=> 'Correo',
    'Already registered?'=>'Ya estas registrado?',
    'Remember me'=>'Recuerdame',
    'Forgot your password?'=>'Has olvidado la contraseña?'

];

?>
