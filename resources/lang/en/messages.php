<?php
return[
    'Welcome' => 'Welcome',
    'Register' => 'Register',
    'Name'=>'Name',
    'Surname'=>'Surname',
    'Password'=>'Password',
    'Confirm Password' => 'Confirm Password',
    'Email'=> 'Email',
    'Already registered?'=>'Already registered?',
    'Remember me'=>'Remember me',
    'Forgot your password?'=>'Forgot your password?'

];

?>
