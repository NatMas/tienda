<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('products')->insert([
            'name' => 'product_name',
            'price' => 122.55,
            'amount' => 55,
            'description' => 'description product',
            'image' => base64_encode(file_get_contents('/home/itb/Descargas/imgprueba.jpg')),
        ]);

    }
}
