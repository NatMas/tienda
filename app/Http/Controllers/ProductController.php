<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Cart_Items;
use App\Models\Product;


use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Log;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function show(Request $request)
    {



        $value = empty($request->input('price')) ? '0' : $request->input('price');

        $all = DB::select(DB::raw("select * from products where price > $value"));

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($all);
        $perPage = 3;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $products = new LengthAwarePaginator($currentPageItems, count($itemCollection), $perPage);
        $products->setPath($request->url());

        return view('products')->with('products', $products);
    }

    public function preu(Request $request)
    {
        $value = empty($request->input('price')) ? '0' : $request->input('price');

        $all = DB::select(DB::raw("select * from products where price = $value"));

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($all);
        $perPage = 3;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $products = new LengthAwarePaginator($currentPageItems, count($itemCollection), $perPage);
        $products->setPath($request->url());

        return view('products')->with('products', $products);
    }

    public function minimo(Request $request)
    {
        $value = empty($request->input('price')) ? '0' : $request->input('price');

        $all = DB::select(DB::raw("select * from products where price >= $value"));

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($all);
        $perPage = 3;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $products = new LengthAwarePaginator($currentPageItems, count($itemCollection), $perPage);
        $products->setPath($request->url());

        return view('products')->with('products', $products);
    }
    public function maximo(Request $request)
    {
        $value = empty($request->input('price')) ? '0' : $request->input('price');

        $all = DB::select(DB::raw("select * from products where price <= $value"));

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($all);
        $perPage = 3;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $products = new LengthAwarePaginator($currentPageItems, count($itemCollection), $perPage);
        $products->setPath($request->url());

        return view('products')->with('products', $products);
    }



    public function addToCart($id)
    {
        /*$products = DB::table('carts')->get();

        if($products->count()>0){
            session()->put('cart', json_decode($products,true));
        }*/

        $product = Product::find($id);
        /*
        Log::error('error');
        Log::warning('warning');
        Log::info('info');
        */
        Log::channel('buy')->info('ejemplo' . $id);


        if (!$product) {
            abort(404);
        }
        $user = Auth::user()->id;
        $cart_bd = Cart::where("user_id", $user)->first();

        if (empty($cart_bd)) {
            $cart_bd = new Cart();
            $cart_bd->user_id = $user;
            $cart_bd->save();
        }

        if (!$product) {
            abort(404);
        }
        $cart = session()->get('cart');

        if (!$cart) {
            $cart = [
                $id => [
                    "name" => $product->name,
                    "amount" => 1,
                    "price" => $product->price,
                    "image" => $product->image
                ]
            ];

            $cart_items = new Cart_Items();
            $cart_items->cart_id = $cart_bd->id;
            $cart_items->product_id = $id;
            $cart_items->amount = 1;
            $cart_items->save();

            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }

        if (isset($cart[$id])) {

            $cart[$id]['amount']++;
            $cart_items = Cart_Items::where([['cart_id', $cart_bd->id], ['product_id', $id]])->first();
            $cart_items->amount = $cart[$id]['amount'];
            $cart_items->update();

            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }

        $cart[$id] = [
            "name" => $product->name,
            "amount" => 1,
            "price" => $product->price,
            "image" => $product->image
        ];

        $cart_items = new Cart_Items();
        $cart_items->cart_id = $cart_bd->id;
        $cart_items->product_id = $id;
        $cart_items->amount = 1;
        $cart_items->save();

        session()->put('cart', $cart);
        return redirect()->back()->with('success', 'Product added to cart successfully!');
    }

    public function cart()
    {
        return view('cart');
    }

    public function update(Request $request)
    {
        if ($request->id && $request->quantity) {
            $cart = session()->get('cart');
            $cart[$request->id]["amount"] = $request->quantity;

            $user = Auth::user()->id;
            $cart_bd = Cart::where('user_id', $user)->first();

            $cart_items = Cart_Items::where([['cart_id', $cart_bd->id], ['product_id', $request->id]])->first();
            $cart_items->amount = $request->quantity;
            $cart_items->update();

            session()->put('cart', $cart);
            session()->flash('success', 'Cart updated successfully');
        }
    }

    public function venta()
    {
        $count = 0;
        $products = DB::table('cart_items')->get();
        for ($i=0;$i<$products->count();$i++){
            $amount=$products[$i]->amount;
            $p=Product::find($products[$i]->product_id);
            if ($amount > $p->stock){
                 $count++;
            }
        }
        if ($count>0){
            session()->flash('success','Estas intentando comprar mas del stock actual');
        }else{
            $user = Auth::user()->id;
            $cart_db = Cart::where("user_id", $user)->first();
            $cart_db::getQuery()->delete();

            session()->remove('cart');
            session()->flash('success', 'Compra finalizada');
        }
    }

    public function remove(Request $request)
    {
        if ($request->id) {
            $cart = session()->get('cart');
            if (isset($cart[$request->id])) {
                unset($cart[$request->id]);

                $user = Auth::user()->id;
                $cart_bd = Cart::where('user_id', $user)->first();
                $cart_items = Cart_Items::where([['cart_id', $cart_bd->id], ['product_id', $request->id]])->first();
                $cart_items->delete();

                session()->put('cart', $cart);
            }
            session()->flash('success', 'Product removed successfully');
        }
    }

}
