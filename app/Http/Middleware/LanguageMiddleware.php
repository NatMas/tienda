<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class LanguageMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        //error_log('route'.app()->getLocale());
        app()-> setLocale(session('lang'));
        //error_log('route'.app()->getLocale());
        return $next($request);
    }
}
