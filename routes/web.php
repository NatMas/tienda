<?php

use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/products',function (){
    $products = App\Models\Product::all();
    //dd($products);
    return view('products')->with('products',$products);
});

//Buscar por precio
Route::get('/products', [ProductController::class,'show'])->name('products');

Route::get('/productspreu', [ProductController::class,'preu'])->name('products');

//Minimo
Route::get('/productsmin', [ProductController::class,'minimo'])->name('products');

//Maximo
Route::get('/productsmax', [ProductController::class,'maximo'])->name('products');



//Carro compra
Route::get('add-to-cart/{id}', [ProductController::class,'addToCart']);

Route::get('cart', [ProductController::class,'cart']);

//Ruta update y remove
Route::patch('update-cart',[ProductController::class,'update']);
Route::patch('empty-from-cart',[ProductController::class,'venta']);
Route::delete('remove-from-cart',[ProductController::class,'remove']);


Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::get('lang/{lang}',function ($lang){
    session(['lang'=>$lang]);
    App::setLocale($lang);
    //error_log('route'.app()->getLocale());
    return redirect()->back();
});

//Ruta ventas
Route::get('venta', 'ProductsController@venta')->middleware('venta');


